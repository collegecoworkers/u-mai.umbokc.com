//backbone module
(function(module) {

  // Dependencies
  var Uploads = window.App.module('uploads');
  var Uploader = window.App.module('uploader');
  var Dashboard = window.App.module('dashboard');
  var App = window.App;
  
  // defines initialize method
  module.initialize = function() {
  
  }; // end initialize
  
  // main upload view
  module.Views.main = Backbone.View.extend({
    
    el: '#sidebar',
    
    initialize: function() {
      
      // creates the spacebar handler
      this.spacebar = new module.Views.space();
      
      // creates categories navigation handler
      this.navigator = new module.Views.navigation();
      
      // creates categories navigation handler
      this.multiple = new module.Views.multiple();
      
    },
    
    // dom events
    events: {
    },
    
    upload: function(e) {
    }
    
  }); // end main view
  
  // spacebar view
  module.Views.space = Backbone.View.extend({
    
    el: '#spacebar',
    
    used_percentage: 0,
    
    initialize: function() {
    
      this.usedSpaceChanged();
      
    },
    
    // changes bar value
    usedSpaceChanged: function() {
    
      this.used_percentage = (App.user.used_space * 100) / App.user.max_space;
      
      $(this.el).find('.value').css('width', (this.used_percentage + '%'));
      
      $(this.el).find('.used_space').html(formatFileSize(App.user.used_space));
      
    }
    
  }); // end space view
  
  // categories navigation view
  module.Views.navigation = Backbone.View.extend({
    
    el: '#navigation',
    
    events: {
      'click a': 'filter'
    },
    
    initialize: function() {
      
    },
    
    // changes bar value
    filter: function(e) {
      
      window.location.hash = ($(e.target).attr('href')).replace("/"+App.settings.dir, "");
      
      e.preventDefault();
      
      
    }
    
  }); // end categories view
  
  // multiple selected view
  module.Views.multiple = Backbone.View.extend({
    
    el: '#multipleselected',
    
    initialize: function() {
      
    },
    
    events: {
      'click .button.delete': 'delete'
    },
    
    show: function() {
      
      $(this.el).show();
      
      this.selected_uploads = Dashboard.main.uploads.collection.getSelected();
      
      $(this.el).find('.title').text(this.selected_uploads.length + ' selected uploads');
      
    },
    
    hide: function() {
    
      $(this.el).hide();
      
      this.selected_uploads = null;
      
    },
        
    delete: function(e) {
      
      _.each(this.selected_uploads, function(model) {
        
        model.destroy({ wait: true });
        
      });
      
    }
    
  });
  
    
})(window.App.module('sidebar'));