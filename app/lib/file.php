<?php

class FileInfo extends \SplFileInfo
{

  /**
  * Lookup hash to convert file units to bytes
  * @var array
  */
  protected static $units = array(
    'b' => 1,
    'k' => 1024,
    'm' => 1048576,
    'g' => 1073741824
  );
  
  /**
  * File
  * @var string
  */
  protected $file;
  
  /**
  * File extension (without leading dot)
  * @var string
  */
  protected $extension;
  
  /**
  * File mimetype (e.g. "image/png")
  * @var string
  */
  protected $mimetype;
  
  /**
  * File size
  * @var int
  */
  protected $size;
  
  /**
  * Initial file type
  * @var string
  */
  protected $initial_type;
  
  /**
  * File type
  * @var string
  */
  protected $type;
  
  /**
  * Secure file location
  * @var string
  */
  protected $secure_location;
  
  /**
  * Constructor
  */
  public function __construct($file)
  {
  
    parent::__construct($file);
    
    $this->file = $file;
    $this->type = 'other';
    $this->name = parent::getFilename();
    
    $this->secure_location = md5(mt_rand(0,1000) . implode(getdate()) . $this->name) . '-' . $this->name;
    
  }
  
  /**
  * Get file extension (without leading dot)
  * @return string
  */
  public function getExtension()
  {
    return strtolower(pathinfo($this->file, PATHINFO_EXTENSION));
  }
  
  /**
   * Get file upload name
   * @return string
   */
  public function getUploadName()
  {
    return $this->secure_location;
  }
  
  /**
  *  Get file name from secure location in file name
  */
  public function getNameFromSecureLocation() {
  
    return substr(strstr($this->name, '-'), 1);
    
  }
  
  /**
  * Get mimetype
  * @return string
  */
  public function getMimetype()
  {
  
    if (!isset($this->mimeType)) {
    
      $finfo = new \finfo(FILEINFO_MIME);
      
      $mimetype = $finfo->file($this->file);
      
      $mimetypeParts = preg_split('/\s*[;,]\s*/', $mimetype);
      
      $this->mimetype = strtolower($mimetypeParts[0]);
      
      unset($finfo);
      
    }
    
    return $this->mimetype;
    
  }
  
  /**
  * Assigns a file category
  * @return string
  */
  public function getFileType()
  {
  
    $type = $this->getMimetype();
    
    // image
    if(strstr($type, 'image')) {
        $this->type = 'image';
    }
    
    // video
    if(strstr($type, 'video')) {
        $this->type = 'video';
    }
    
    // audio
    if(strstr($type, 'audio')) {
        $this->type = 'audio';
    }
    
    $doc_list = array('pdf', 'text', 'word', 'excel', 'pages', 'numbers');
    if(stringContainsArray($type, $doc_list) || stringContainsArray($this->initial_type, $doc_list) || in_array($this->getExtension(), $doc_list)) {
      $this->type = 'document';
    }
    
    return $this->type;
    
  }
  
  /**
  *  Returns supports thumbnail bool
  */
  function supportsThumbnail() {
  
    $thumbnail_extensions = array('jpg', 'jpeg', 'png', 'gif', 'bmp');
    if( ($this->getFileType() == 'image') && in_array($this->getExtension(), $thumbnail_extensions)) {
    
      return true;
      
    }
    
    return false;
    
  }
  
  /**
  *  Copies file to new location
  */
  public function move($location)
  {
  
    if(file_exists($this->file)) {
      
      if(copy($this->file, $location)) {
      
        unlink($this->file);
        
        return true;
        
      }
      
    }
    
    return false;
  
  }
  
  /**
  *  Deletes file
  */
  public function delete() {
    
    if(file_exists($this->file)) {
      
      if(unlink($this->file)) {
        
        return true;
        
      }
      
    }
    
    return false;
    
  }
  
}

?>