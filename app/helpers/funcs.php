<?php

function is_admin(){
	if(isset($_SESSION['user']))
		return $_SESSION['user']->role == 'admin';
	else
		return false;
}

function generate_users_sidebar($users, $active_user_id){
	$res = '<ul>';
	$users = json_decode($users);
	foreach ($users as $user) {
		$active = $active_user_id == $user->id ? 'active' : '';
		$res .= '<li class="'.$active.'"><a href="/user/'.$user->id.'"> <span class="icon users"></span> '.$user->username.' <span class="counter">'.$user->uploads_count.'</span></a></li>';
	}
	$res .= '</ul>';
	return $res;
}

function check_user($id){
	$app = \Slim\Slim::getInstance();
	if($app->sessions->get_var('user') === null) return false;
	$user = User::find_by_id($id);
	return $user !== null;
}