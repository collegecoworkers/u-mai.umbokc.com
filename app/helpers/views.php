<?php

/**
*  Generates file categories html list
*/
function generate_categories() {
  
  $html = "<ul>".PHP_EOL;
  
  $app = \Slim\Slim::getInstance();
  
  $categories = array(
    'all' => 'Все',
    'audio' => 'Аудио',
    'image' => 'Картинки',
    'video' => 'Видео',
    'document' => 'Документы',
    'other' => 'Другое'
  );
  
  // generates each category html
  foreach($categories as $category => $title) {
    $class = ($category == 'Все') ? "active" : "";
    $href = $app->urlFor('uploads_pagination', array( 'page' => 1, 'filter' => $category));
    $html .= '<li data-type="'.$category.'" class="'.$class.'"><a href="'.$href.'"> <span class="icon '.$category.'"></span> '.$title.' <span class="counter">12</span> </a></li>'.PHP_EOL;
    
  }
  
  $html .= "<ul>";
  
  return $html;
  
}

/**
*  Generates upload url
*/
function get_url($upload) {
  
  if($upload->url) {
    return $upload->url;
  }

  return app_url()."/".$upload->uid;
  
}

/**
*  Returns upload protected status
*/
function is_protected($upload) {
  
  if(!$upload->password || ($upload->password && check_upload_access($upload->uid))) {
    
    return false;
    
  }
  
  return true;
  
}

/**
*  Returns json from flash messages
*/
function get_flashMessages() {
  
  $app = \Slim\Slim::getInstance();
  
  $flash = $app->view()->getData('flash');
  
  $messages = $flash->getMessages();
  
  $response = array();
  
  foreach($messages as $key => $val) {
    
    $response[] = array('type' => $key, 'message' => $val);
    
  }
  
  return json_encode($response);
  
}

/**
*  Returns settings data schema
*/
function get_settingsSchema($key = '') {
  
  $schema = array(
    
    'admin_email' => array( 'label' => __('Электронная почта администратора'), 'type' => 'text'),
    
    'appname' => array( 'label' => __('Имя приложения'), 'type' => 'text'),
    
    'url' => array( 'label' => __('URL-Адрес приложения, <small>без http(s)://</small>'), 'type' => 'text'),
    
    'timezone' => array( 'label' => __('Часовой пояс'), 'type' => 'select'),
    
    'default_maxspace' => array( 'label' => __('Максимальное пространство для пользователя (байт)'), 'type' => 'int'),
    
    'uploads_maxupload' => array( 'label' => __('Максимальный размер загружаемого файла (байт)'), 'type' => 'int'),
    
    'uploads_maxsimuploads' => array( 'label' => __('Максимальное число одновременных загрузок'), 'type' => 'int'),
    
    'uploads_perpage' => array( 'label' => __('Загрузка на страницу'), 'type' => 'int')
    
  );
  
  if($key) {
  
    return $schema[$key];
    
  }
  
  return $schema;
  
}

/**
*  Returns upload preview capability
*/
function upload_supports_preview($upload) {

  if($upload->thumbnail != 'none') {
  
    return true;
    
  }
  
  return false;
  
}

?>