<?php

// stores a list of all messages and labels used in the app
$localization_dict = array(
  
  // Dashboard Messages
  'upload_space' => "Недостаточно места для загрузки одного или нескольких выбранных файлов",
  'upload_big'   => "Файл слишком большой для загрузки",
  'upload_small' => "Файл слишком мал для загрузки",
  
  
  // Login views
  'login_register'     => "Регистрация",
  'login_login'        => "Вход",
  'login_reset'        => "Сброс",
  
  // Login Messages
  'login_invalidemail' => "Пожалуйста, введите действительный адрес электронной почты ",
  'login_invaliduser'  => "Пожалуйста, вводите только цифры и буквы в имени пользователя",
  'login_emptyfields'  => "Пожалуйста, заполните все необходимые поля",
  'login_passwordmatch'=> "Пароли не совпадают",
  'login_wronglogin'   => "Неверное Имя Пользователя / Пароль",
  'login_takenemail'   => "Выбранная почта уже зарегистрирована",
  'login_takenuser'    => "Выбранное имя пользователя уже зарегистрировано",
  'login_emailexist'   => "Адрес электронной почты не зарегистрирован",
  'login_reset'        => "На вашу почту отправлено письмо с инструкциями",
  'login_resetsucess'  => "Пароль был успешно изменен",
  
  // Upload messages
  'uploap_ftpnotice'   => "Обнаружена новая загрузка FTP.",
  'upload_wrongpass'   => "Неверный пароль",
  
  // Upload errors
  'File size is too large' => "Размер файла слишком велик",
  'No file was uploaded'   => "Файл не был загружен",
  
  // install errors
  'install_dberror'    => "Не удалось подключиться к базе данных"
  
);

?>