<div id="account" class="modal">

  <div class="overlay">
  
    <div class="dialog animated slideIn">
      
      <div class="content clearfix">
        
        <h2 class="title">Настройки Учетной Записи</h2>
        
        <!--Avatar-->
        <div class="field editavatar">
        
          <div class="avatar">
          
            <div class="shadow">
              
              <% if(avatar) {  %>
            
                <img src="<%= avatar %>">
                
              <% } else { %>
              
                <?php image_tag('avatar.png'); ?>
              
              <% } %>
              
            </div>
            
          </div>
          
          <a class="button">Изменить</a>
        
        </div>
        
        <!--Avatar-->
        <div class="field avatar_field">
          <label>Аватар</label>
          <input type="text" name="avatar" value="<%= avatar %>">
        </div>
        
        <!--Email-->
        <div class="field">
          <label>Email</label>
          <input type="email" name="email" value="<%= email %>">
        </div>
        
        <!--Username-->
        <div class="field">
          <label>Логин</label>
          <input type="text" name="username" value="<%= username %>">
        </div>
        
        <?php if($user->hasRights()) : ?>
        
        <!--Max space-->
        <div class="field">
          <label>Доступное пространство</label>
          <input type="number" class="digitonly" name="max_space" value="<%= max_space %>">
        </div>
        
        <?php endif; ?>
        
        <!--Password-->
        <div class="field">
          <label>Изменить пароль</label>
          <input type="password" name="password">
        </div>
        
        <!--Password-->
        <div class="field">
          <label>Подтверждение пароля</label>
          <input type="password" name="password_confirm">
        </div>
        
        <div class="message"></div>
        
      </div>
      
      <div class="bar clearfix">
      
        <a class="button close right">Закрыть</a>
        
        <a class="button blue save right">Сохранить</a>
      
      </div>
      
    </div>
    
  </div>
</div>