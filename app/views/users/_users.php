<div id="users" class="modal">

  <div class="overlay">
  
    <div class="dialog animated slideIn">
      
      <div class="content">
        
        <h2 class="title">Пользователей: <%= count %></h2>
        
        <div class="users">
        </div>
        
        <label> Поиск по электронной почте или имени пользователя</label>
        
        <input type="text" name="search" class="search" placeholder="user@example.com или john" autofocus>
        
      </div>
      
      <div class="bar clearfix">
      
        <a class="button close right">Закрыть</a>
      
      </div>
      
    </div>
    
  </div>
  
</div>