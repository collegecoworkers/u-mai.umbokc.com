<!--Navigation-->
<div class="navigation section">

	<!--Title-->
	<h2 class="title"> Категории <span class="desc"> - По типу файлов </span>
	</h2>

	<!--Content-->
	<div class="content">
		
		<?php echo generate_categories(); ?>

	<!--End .content-->
	</div>

<!--End .section-->
</div>

<?php if (is_admin()): ?>

<!--Navigation-->
<div class="navigation section users-nav">

	<!--Title-->
	<h2 class="title"> Файлы пользователей </h2>

	<!--Content-->
	<div class="content">
		
		<?php echo generate_users_sidebar($users, $active_user_id); ?>

	<!--End .content-->
	</div>

<!--End .section-->
</div>

<?php endif ?>

<!--Progressbar-->
<div id="spacebar" class="section">

	<!--Title-->
	<h2 class="title"> Пространство <span class="desc"> - Досупное место </span></h2>

	<!--Content-->
	<div class="content">
		
		<div class="progress">
		
			<div class="bar">
			
				<span class="value" style="width: 0%"></span>
				
			</div>
			
			<span class="label"> <span class="used_space"></span> из <span class="max_space"><?php echo formatFileSize($user->max_space); ?></span> </span>
			
		</div>

	<!--End .content-->
	</div>

<!--End .section-->
</div>

<!--Multiple Selection-->
<div id="multipleselected" class="section hidden">

	<!--Title-->
	<h2 class="title"> Выбранные </h2>

	<!--Content-->
	<div class="content">
		
		<a class="button delete">Удалить</a>

	<!--End .content-->
	</div>

<!--End .section-->
</div>