<form method="post" action="<?php url_for('reset_token', array('token' => $token)); ?>">
  
  <!--Password-->
  <p class="password field">
    <input tabindex="2" id="password" type="password" name="password" value="" placeholder="Новый пароль" autofocus />
  </p>
  
  <!--Password-->
  <p class="password_confirm field">
    <input tabindex="2" id="password_confirm" type="password" name="password_confirm" value="" placeholder="Введите новый пароль еще раз" />
  </p>
  
  <!--Submit-->
  <input type="submit" tabindex="5" class="button blue" value="Сбросить" />

</form>