<form method="post" action="<?php url_for('reset'); ?>">
  
  <p class="message">
    
    На вашу почту будет отправлено письмо с инструкциями по восстановлению пароля.
    
  </p>
  
  <!--Email-->
  <p class="email field">
    <input tabindex="1" id="email" type="text" name="email" value="<?php if(isset($_POST['email'])) echo $_POST['email']; ?>" placeholder="email@example.com" autofocus />
    <a href="<?php url_for('login'); ?>" tabindex="3" class="action button register" title="Нажмите, чтобы войти в систему с существующей учетной записью.">Войти</a>
  </p>
  
  <!--Submit-->
  <input type="submit" tabindex="5" class="button blue" value="Сбросить" />

</form>