<form method="post" action="<?php url_for('login'); ?>">
  
  <!--Email-->
  <p class="email field">
    <input tabindex="1" id="email" type="text" name="email" value="<?php if(isset($_POST['email'])) echo $_POST['email']; ?>" placeholder="email или логин" autofocus />
    <a href="<?php url_for('register'); ?>" tabindex="3" class="action button register" title="Нажмите, для регистрации учетной записи пользователя.">Регистрация</a>
  </p>
  
  <!--Password-->
  <p class="password field">
    <input tabindex="2" id="password" type="password" name="password" value="" placeholder="Пароль" />
    <a href="<?php url_for('reset'); ?>" tabindex="4" class="action button reset" title="Нажмите, чтобы получить инструкции по сбросу пароля на указанный адрес электронной почты.">Сбросить</a>
  </p>
  
  <!--Submit-->
  <input type="submit" tabindex="5" class="button blue" value="Войти" />

</form>